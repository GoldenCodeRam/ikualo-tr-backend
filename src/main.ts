import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { ValidationPipe } from "@nestjs/common";
import * as cookieParser  from "cookie-parser";

async function bootstrap() {
    const app = await NestFactory.create(AppModule, {
        bufferLogs: true,
    });

    // Enable CORS with credentials for testing.
    app.enableCors({ origin: "http://localhost:5173", credentials: true });

    // Use class-validator package for endpoint validation.
    // See more: https://docs.nestjs.com/techniques/validation
    app.useGlobalPipes(new ValidationPipe());

    // Use Cookies for JWT Authentication.
    app.use(cookieParser());

    await app.listen(3000);
}

bootstrap();
