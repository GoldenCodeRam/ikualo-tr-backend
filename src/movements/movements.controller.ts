import {
    Body,
    Controller,
    Delete,
    Get,
    Post,
    Request,
    UseFilters,
} from "@nestjs/common";
import { MovementsService } from "./movements.service";
import { JwtPayload, RequestWithJwtPayload } from "src/auth/auth.service";
import { CreateMovementDto, DeleteMovementDto } from "./movementsDto";
import { UsersService } from "src/users/users.service";
import { PrismaClientKnownRequestErrorFilter } from "src/database/prismaClientKnownRequestError.filter";

@Controller("movements")
export class MovementsController {
    constructor(
        private movementsService: MovementsService,
        private usersService: UsersService,
    ) {}

    @Get()
    async findAll(@Request() request: RequestWithJwtPayload) {
        return await this.movementsService.findAll(request.payload.sub);
    }

    @Post()
    async create(
        @Request() request: RequestWithJwtPayload,
        @Body() dto: CreateMovementDto,
    ) {
        const user = await this.usersService.findOne(request.payload.sub);
        return await this.movementsService.create(user, dto);
    }

    @Delete()
    @UseFilters(new PrismaClientKnownRequestErrorFilter())
    async delete(@Body() dto: DeleteMovementDto) {
        return await this.movementsService.delete(dto);
    }

    @Get("/total-capital")
    async getCapital(@Request() request: RequestWithJwtPayload) {
        return await this.movementsService.getTotalCapital(request.payload.sub);
    }
}
