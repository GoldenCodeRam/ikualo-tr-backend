import {
    IsIn,
    IsNotEmpty,
    IsNumber,
    IsPositive,
    IsString,
} from "class-validator";

const MovementType = {
    "IN": "IN",
    "OUT": "OUT",
} as const;
export type MovementType = keyof typeof MovementType;

export class CreateMovementDto {
    @IsNotEmpty()
    @IsIn(Object.keys(MovementType))
    type: string;

    @IsNotEmpty()
    @IsPositive()
    @IsNumber({ maxDecimalPlaces: 2 })
    value: number;

    description?: string;
}

export class DeleteMovementDto {
    @IsNotEmpty()
    @IsString()
    id: string;
}
