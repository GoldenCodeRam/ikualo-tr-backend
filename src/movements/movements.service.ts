import { Injectable, Logger } from "@nestjs/common";
import { Movement, PrismaClient, User } from "@prisma/client";
import {
    CreateMovementDto,
    DeleteMovementDto,
    MovementType,
} from "./movementsDto";

const client = new PrismaClient({ log: [{ level: "query", emit: "event" }] });

@Injectable()
export class MovementsService {
    private readonly logger = new Logger(MovementsService.name);

    constructor() {
        client.$on("query", (event) => {
            this.logger.log(JSON.stringify(event));
        });
    }

    async findAll(email: string) {
        this.logger.log(`Finding all movements for user: ${email}`);

        return await client.movement.findMany({
            where: {
                user: {
                    email,
                },
            },
        });
    }

    async getTotalCapital(email: string) {
        this.logger.log(`Calculating total capital for user: ${email}`);

        let total = 0;
        const movements = await client.movement.findMany({
            where: {
                user: {
                    email,
                },
            },
        });

        movements.forEach((movement) => {
            if ((movement.type as MovementType) === "IN") {
                return (total += movement.value);
            }
            return (total -= movement.value);
        });

        return {
            total,
        };
    }

    async create(user: User, dto: CreateMovementDto): Promise<Movement> {
        this.logger.log(`Creating new movement for user: ${user.email}`);

        return await client.movement.create({
            data: {
                userId: user.id,
                ...dto,
            },
        });
    }

    async delete(dto: DeleteMovementDto): Promise<Movement> {
        this.logger.log(`Deleting movement: ${dto.id}`);

        return await client.movement.delete({
            where: {
                id: dto.id,
            },
        });
    }
}
