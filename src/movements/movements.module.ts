import { Module } from "@nestjs/common";
import { MovementsService } from "./movements.service";
import { MovementsController } from "./movements.controller";
import { AuthModule } from "src/auth/auth.module";
import { UsersModule } from "src/users/users.module";

@Module({
    imports: [AuthModule, UsersModule],
    providers: [MovementsService],
    controllers: [MovementsController],
})
export class MovementsModule {}
