import {
    ArgumentsHost,
    Catch,
    ExceptionFilter,
    HttpStatus,
    Logger,
} from "@nestjs/common";
import { PrismaClientKnownRequestError } from "@prisma/client/runtime/library";
import { Response } from "express";

@Catch(PrismaClientKnownRequestError)
export class PrismaClientKnownRequestErrorFilter implements ExceptionFilter {
    private readonly logger = new Logger(
        PrismaClientKnownRequestErrorFilter.name,
    );

    catch(exception: PrismaClientKnownRequestError, host: ArgumentsHost) {
        this.logger.error(`${exception.code}: ${exception.message}`);

        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();

        switch (exception.code) {
            case "P2002":
                return response
                    .status(HttpStatus.CONFLICT)
                    .json({ meta: exception.meta });
            case "P2025":
                return response
                    .status(HttpStatus.NOT_FOUND)
                    .json({ meta: exception.meta });
            default:
                return response
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .json({ error: "Unknown error!" });
        }
    }
}
