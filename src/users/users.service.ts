import { Injectable } from '@nestjs/common';
import { PrismaClient, User } from '@prisma/client';

import { hash } from 'bcrypt';
import { Logger } from '@nestjs/common';

const client = new PrismaClient({ log: [{ level: 'query', emit: 'event' }] });

@Injectable()
export class UsersService {
  private readonly logger = new Logger(UsersService.name);

  async findOne(email: string): Promise<User | undefined> {
    this.logger.log(`Finding user with email: ${email}`);

    return await client.user.findUnique({
      where: {
        email,
      },
    });
  }

  async create(user: { email: string; password: string }): Promise<User> {
    // Logging events.
    this.logger.log(`Creating new user: ${user.email}`);
    client.$on('query', (event) => {
      this.logger.log(JSON.stringify(event));
    });

    return await client.user.create({
      data: {
        email: user.email,
        password: await hash(
          user.password,
          parseInt(process.env.BCRYPT_SALT_ROUNDS),
        ),
      },
    });
  }
}
