import {
    Body,
    Controller,
    Get,
    HttpCode,
    HttpStatus,
    Post,
    Request,
    Res,
    UseFilters,
    UseGuards,
} from "@nestjs/common";
import { AuthService, JwtPayload, RequestWithJwtPayload } from "./auth.service";
import { LoginDto, RegisterDto } from "./authDto";
import { PrismaClientKnownRequestErrorFilter } from "src/database/prismaClientKnownRequestError.filter";
import { AuthGuard, Public } from "./auth.guard";
import { Response } from "express";

@Controller("auth")
export class AuthController {
    constructor(private authService: AuthService) {}

    @Public()
    @HttpCode(HttpStatus.OK)
    @Post("login")
    async signIn(
        @Body() loginDto: LoginDto,
        @Res({ passthrough: true }) response: Response,
    ) {
        const token = await this.authService.signIn(
            loginDto.email,
            loginDto.password,
        );

        response.cookie("token", token.access_token, {
            sameSite: "none",
            secure: true,
            httpOnly: true,
            partitioned: true,
        });

        return {
            sub: loginDto.email,
        };
    }

    @Public()
    @HttpCode(HttpStatus.CREATED)
    @Post("register")
    @UseFilters(new PrismaClientKnownRequestErrorFilter())
    async register(@Body() registerDto: RegisterDto) {
        return await this.authService.register(
            registerDto.email,
            registerDto.password,
        );
    }

    @UseGuards(AuthGuard)
    @Get("me")
    async me(@Request() request: RequestWithJwtPayload) {
        return request.payload;
    }
}
