import {
    Injectable,
    Logger,
    Request,
    NotFoundException,
    UnauthorizedException,
} from "@nestjs/common";
import { UsersService } from "src/users/users.service";

import { compare } from "bcrypt";
import { JwtService } from "@nestjs/jwt";
import { User } from "@prisma/client";

export type JwtPayload = {
    sub: string;
};
export type RequestWithJwtPayload = Request & { payload: JwtPayload };

@Injectable()
export class AuthService {
    private readonly logger = new Logger(AuthService.name);

    constructor(
        private usersService: UsersService,
        private jwtService: JwtService,
    ) {}

    async signIn(email: string, password: string) {
        this.logger.log(`Attempt sign in: ${email}`);

        const user = await this.usersService.findOne(email);

        if (!user) {
            throw new NotFoundException();
        }

        if (await compare(password, user.password)) {
            return await this.getJwtToken(user);
        }

        throw new UnauthorizedException();
    }

    async register(email: string, password: string) {
        this.logger.log(`Attempt register: ${email}`);

        const user = await this.usersService.create({ email, password });

        return await this.getJwtToken(user);
    }

    async getJwtToken(user: User) {
        const payload: JwtPayload = { sub: user.email };

        return {
            access_token: await this.jwtService.signAsync(payload),
        };
    }
}
