#!/bin/bash

echo "Starting docker compose..."
docker compose -f ./docker-compose.yaml up -d

echo "Waiting 5s for MongoDB Instance to startup..."
sleep 5

echo "Starting MongoDB Replication for Prisma..."
docker exec mongo mongosh -u root -p root --eval "rs.initiate()"

echo "Done! MongoDB and Mongo Express should be online!"
exit
