import { PrismaClient } from "@prisma/client";
import { hash } from "bcrypt";

const prisma = new PrismaClient();
async function main() {
    await prisma.user.create({
        data: {
            email: "test@test.com",
            password: await hash(
                "test",
                parseInt(process.env.BCRYPT_SALT_ROUNDS),
            ),
        },
    });
}

main().finally(async () => {
    prisma.$disconnect();
});
