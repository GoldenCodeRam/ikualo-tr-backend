# ikualo-tr-backend

A continuación se presenta la solución al desafío técnico ofrecido para la posición de _Programador Junior_. Esta solución se encuentra dividida en dos proyectos; _Backend_ y _Frontend_. Este es el proyecto _Backend_.

## Requerimientos del Proyecto

A continuación se muestran los requerimientos directos del proyecto:

 - Crear una API REST para gestionar los movimientos financieros.
 - Implementar operaciones CRUD para los movimientos financieros.
 - Utilizar una base de datos MongoDB para almacenar los movimientos.
 -  Mantener actualizado el capital del usuario.

Por otra parte, se especificó que se debía utilizar el _framework_ [Nest.js](https://nestjs.com/), para el desarrollo del servidor.

## Funcionalidades del Proyecto.

A continuación se presentan las funcionalidades del proyecto como una lista de tareas. Se decide usar este formato porque estas funcionalidades puede que no se encuentren desarrolladas en su totalidad, puedan requerir cambios o se hayan implementado con el fin de mostrar posibles mejoras a futuro. La descripción de otras funcionalidades y características del proyecto no enunciadas en los requerimientos directos del proyecto se mostrarán en la sección de [Funcionalidades Adicionales del Proyecto](#funcionalidades-adicionales-del-proyecto).

 - [x] API tipo REST para el manejo de los movimientos financieros.
   - [x] Crear movimientos.
   - [x] Leer movimientos.
   - [ ] Actualizar movimientos.
   - [x] Eliminar movimientos.
 - [x] Uso de [MongoDB](https://www.mongodb.com/) como base de datos.
 - [x] El capital del usuario se calcula constantemente, dependiendo de las solicitudes del usuario.

## Funcionalidades Adicionales del Proyecto.

A continuación se presentan las funcionalidades adicionales del proyecto como una lista de tareas. Se decide usar este formato porque estas funcionalidades puede que no se encuentren desarrolladas en su totalidad, puedan requerir cambios o se hayan implementado con el fin de mostrar posibles mejoras a futuro.

 - [x] _Logs_.
 - Usando la librería _Nest.js_, se realizan _logs_ de las acciones con regularidad. Esto con el fin de encontrar posibles errores en la aplicación.
 - Se podrían implementar otras librerías u otras formas de realizar este control.
 - [x] [Prisma](https://www.prisma.io/).
 - Se hace uso del ORM como ODM, [Prisma](https://www.prisma.io/). Se decidió usar esta librería como preferencia sobre [Mongoose.js](https://mongoosejs.com/), debido a la facilidad de su cliente, la estructura del proyecto, la documentación y el manejo de errores.
 - La elección de esta librería requirió realizar algunos cambios en el despliegue de la base de datos, esta información se verá en la sección de [Despliegue](#despliegue).
 - [x] Autenticación con [JWT](https://jwt.io/).
 - [ ] Pruebas Unitarias.
 - No se tendrán en cuenta para la entrega.
 - [ ] Pruebas Funcionales.
 - No se tendrán en cuenta para la entrega.

## Despliegue

Este proyecto, a diferencia del proyecto _Frontend_, posee más requerimientos y dependencias para que todo funcione correctamente. Así, se hará una lista de dependencias al igual que el proyecto _Frontend_.

 > Al igual que con el proyecto _Frontend_ se está utilizando el gestor de paquetes [Nix](https://nixos.org/), pero se pueden instalar las dependencias directamente si no se desea utilizar este gestor.

### Dependencias

A continuación se muestra la información de la computadora utilizada para el desarrollo y despliegue del servidor:

```
                     ./o.                  goldencoderam@goldencoderam-pc 
                   ./sssso-                ------------------------------ 
                 `:osssssss+-              OS: EndeavourOS Linux x86_64 
               `:+sssssssssso/.            Kernel: 6.9.3-arch1-1 
             `-/ossssssssssssso/.          Uptime: 1 hour, 55 mins 
           `-/+sssssssssssssssso+:`        Packages: 929 (pacman), 163 (nix-user), 51 (nix-default) 
         `-:/+sssssssssssssssssso+/.       Shell: bash 5.2.26 
       `.://osssssssssssssssssssso++-      Resolution: 1920x1080, 1920x1080 
      .://+ssssssssssssssssssssssso++:     WM: i3 
    .:///ossssssssssssssssssssssssso++:    Theme: Arc-Dark [GTK3] 
  `:////ssssssssssssssssssssssssssso+++.   Icons: Qogir-dark [GTK3] 
`-////+ssssssssssssssssssssssssssso++++-   Terminal: kitty 
 `..-+oosssssssssssssssssssssssso+++++/`   CPU: Intel i3-6100 (4) @ 3.700GHz 
   ./++++++++++++++++++++++++++++++/:.     GPU: NVIDIA GeForce GTX 960 
  `:::::::::::::::::::::::::------``       Memory: 3729MiB / 11906MiB
```

Dependencias:

```
node v21.7.2
npm v10.5.0
Docker version 26.1.3, build b72abbb6f0
Docker Compose version 2.27.1 
```

### Lanzamiento de la Base de Datos

Para el lanzamiento de la base de datos, se utiliza el sistema de virtualización y creación de contenedores [Docker](https://www.docker.com/).

Para lanzar la base de datos, se ha creado un _script_ que puede ayudar con esta tarea.

```sh
# Cambiar permisos de ejecución para el script
chmod +x ./docker/startup.sh

# Ejecutar el script
cd docker && ./startup.sh
```

Este _script_ es necesario, ya que para poder usar el _ODM Prisma_, se requiere lanzar _MongoDB_ en modo replicación. Ver más en: https://www.mongodb.com/docs/manual/replication/

Una vez se ha hecho el lanzamiento de la base de datos, se procede con la configuración de las variables de entorno del proyecto:

```sh
# Se recomienda copiar el archivo de ejemplo.
cp .env.example .env
```

| Variable | Descripción | Valor por Defecto |
| - | - | - |
| `DATABASE_URL` | Define la URL para la base de datos. | `mongodb://root:root@127.0.0.1:8082/gmb?authSource=admin&directConnection=true` |
| `JWT_SECRET_KEY` | Define la cadena de caracteres para realizar la firma de las llaves JWT. | `<cadena-en-base-64>` |
| `BCRYPT_SALT_ROUNDS` | Usado para encriptar las contraseñas de los usuarios. | `10` |

Ahora, con la base de datos funcionando correctamente y las variables de entorno configuradas, se procede a aplicar el esquema de la base de datos ofrecido por _Prisma_, junto con la adición de un usuario de prueba para manejar la aplicación:

```sh
# Aplicación del esquema usando Prisma.
npx prisma db push

# Seeding de la base de datos.
npx prisma db seed
```

| Email | `test@test.com` |
| - | - |
| Contraseña | `test` |

Finalmente, se puede ejecutar el entorno de desarrollo para observar el funcionamiento de la aplicación:

```sh
npm run start
```
